# Deployment Guide

This document breaks down the process of deploying our _constant time_ prime factorisation (terms and conditions apply) to a Digital Ocean (DO) kubernetes K8s cluster.

## Creating Docker Image

In the root of the directory for a given language include a `Dockerfile`.
The below is an overview of the contents which may require additions for a particular language.

```
// pull the base image
FROM <base image>
// create a directory in the docker container to clone the local data to as the Docker container does not include the project directory by default.
RUN mkdir /var/check-<X>
// set the working directory to that directory
WORKDIR /var/check-<X>
// copy the contents of the project directory into the docker container
COPY ./* ./
// run sam's wrapper
CMD <Include Sam's wrapper>
```

Then run the command `docker build ./ -t <lang>check<p>:<version>`. To create a docker container. It should now appear when you run `docker images`.

## Publish Docker Container to DO Container Registry

To publish to DO by tagging using

```
docker tag <lang>check<p>:<version> registry.digitalocean.com/the-secret-ingredient-is-prime/<lang>check<p>
```

Then

```
docker push registry.digitalocean.com/the-secret-ingredient-is-prime/<lang>check<p>
```

This may require you to login with `doctl registry login`.
`doctl auth list` should show current user as `do_docker` which has the DO api key.

It should then appear in the DO dashboard.

## Deploying to k8s

Create a cluster and link it to the container registry using the inbuilt functionality in the DO dashboard container registry.

See [this youtube video for authentication](https://youtu.be/flQXSbc8hBI?t=826) and linking `kubectl` to the cluster.

Linking the DO container registry to the k8s cluster should have created a secret which can be used to fetch the docker image. Specifically the secret should be visible using `kubectl get secrets`. We will call the id of the secret `<secret>` henceforth.

Then use the following template for the k8s yaml deployment configuration. note as far as i'm aware you can only have one deployment per cluster. However, you can have multiple containers per node.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: <name>
spec:
  selector:
    matchLabels:
      app: <name>

  template:
    metadata:
      labels:
        app: t<name>

    spec:
      containers:
        - name: <name>
          image: registry.digitalocean.com/the-secret-ingredient-is-prime/<name>
        <repeat as many times as required>
      imagePullSecrets:
        - name: <secret>

```

This can be pushed using the command

```
kubectl apply -f <deployment config file>
```

Then go to the dashboard accessible through the k8s dashboard to see if it has deployed.
